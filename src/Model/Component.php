<?php
/**
 * Component Class.
 *
 * PHP Version 7.1+
 *
 * @package Squiz\ElasticIO
 * @author  Scott Kim <skim@squiz.net>
 */
namespace Squiz\ElasticIO\Model;

use Squiz\ElasticIO\Request;
use \Squiz\ElasticIO\Model\Base;

/**
 * Component
 */
class Component extends Base
{

    /**
     * Base API path for Components end-point.
     *
     * @var string
     */
    public $basePath = '/components';

    /**
     * List of API parameters.
     *
     * @var array
     */
    public $parameters = [];


    /**
     * Retrieve all components.
     *
     * @param string $contractid  An Id of the contract.
     * @param array  $filter      Filter setting.
     *
     * @return Component
     * @see    https://api.elastic.io/docs/v2/#retrieve-all-components
     */
    public function retrieveAll(string $contractid, array $filter=[])
    {
        $queryData = ['contract_id' => $contractid];

        foreach ($filter as $key => $val) {
            $queryData['filter['.$key.']'] = $val;
        }

        $this->response = $this->request
            ->setMethod('get')
            ->setPath($this->basePath.'?'.http_build_query($queryData))
            ->execute();
        return $this;

    }//end retrieveAll()


    /**
     * Retrieve a component by ID.
     *
     * @param string $componentid Componentid ID.
     *
     * @return Component
     * @see    https://api.elastic.io/docs/v2/#retrieve-a-component-by-id
     */
    public function retrieveByID(string $componentid)
    {
        $this->response = $this->request
            ->setMethod('get')
            ->setPath($this->basePath.'/'.$componentid)
            ->execute();
        return $this;

    }//end retrieveByID()


}//end class