<?php
/**
 * DataSample Class.
 *
 * PHP Version 7.1+
 *
 * @package Squiz\ElasticIO
 * @author  Scott Kim <skim@squiz.net>
 */
namespace Squiz\ElasticIO\Model;

use Squiz\ElasticIO\Request;
use \Squiz\ElasticIO\Model\Base;

/**
 * DataSample
 */
class DataSample extends Base
{

    /**
     * Base API path for DataSamples end-point.
     *
     * @var string
     */
    public $basePath = '/data-samples';

    /**
     * List of API parameters.
     *
     * @var array
     */
    public $parameters = [
        'create' => [
            'type'                                    => ['required' => true, 'type' => 'string', 'valid' => 'data-sample'],
            'attributes.method'                       => ['required' => true],
            'attributes.result'                       => ['required' => true],
            'relationships.component.data.id'         => ['required' => true],
            'relationships.component_version.data.id' => ['required' => true],
            'relationships.workspace.data.id'         => ['required' => true],
            'relationships.workspace.data.type'       => ['required' => true, 'type' => 'string', 'valid' => 'workspace'],
        ]
    ];


    /**
     * Retrieve a data sample by ID.
     *
     * @param string $dataSampleid Data sample ID.
     *
     * @return DataSample
     * @see    https://api.elastic.io/docs/v2/#retrieve-data-sample
     */
    public function retrieveByID(string $dataSampleid)
    {
        $this->response = $this->request
            ->setMethod('get')
            ->setPath($this->basePath.'/'.$dataSampleid)
            ->execute();
        return $this;

    }//end retrieveByID()


    /**
     * Creates data sample.
     *
     * @param array $data     Parameters to create.
     * @param array $baseData Optional base data to use. Any missing parameter
     *                        in $data can be looked up from here.
     *
     * @return DataSample
     * @see    https://api.elastic.io/docs/v2/#create-data-sample
     */
    public function create(array $data, array $baseData=null)
    {
        if ($baseData !== null) {
            $data = $this->mapData($this->parameters['create'], $data, $baseData);
        }

        $this->validateData($this->parameters['create'], $data);
        $this->response = $this->request
            ->setMethod('post')
            ->setPath($this->basePath)
            ->setData(['data' => $data])
            ->execute();
        return $this;

    }//end create()


}//end class