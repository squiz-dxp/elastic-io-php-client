<?php
/**
 * Flow Class.
 *
 * PHP Version 7.1+
 *
 * @package Squiz\ElasticIO
 * @author  Scott Kim <skim@squiz.net>
 */
namespace Squiz\ElasticIO\Model;

use Squiz\ElasticIO\Request;
use \Squiz\ElasticIO\Model\Base;

/**
 * Flow
 */
class Flow extends Base
{

    /**
     * Base API path for Flowss end-point.
     *
     * @var string
     */
    public $basePath = '/flows';

    /**
     * List of API parameters.
     *
     * @var array
     */
    public $parameters = [
        'create' => [
            'type'                              => ['required' => true, 'type' => 'string', 'valid' => 'flow'],
            'attributes.name'                   => ['required' => true],
            'attributes.type'                   => ['required' => true, 'type' => 'string', 'valid' => ['ordinary', 'long_running']],
            'attributes.graph'                  => ['required' => true],
            'relationships.workspace.data.id'   => ['required' => true],
            'relationships.workspace.data.type' => ['required' => true, 'type' => 'string', 'valid' => 'workspace'],
        ],
        'update' => [
            'type'             => ['required' => true, 'type' => 'string', 'valid' => 'flow'],
            'id'               => ['required' => true],
            'attributes.name'  => ['required' => false],
            'attributes.type'  => ['required' => false, 'type' => 'string', 'valid' => ['ordinary', 'long_running']],
            'attributes.graph' => ['required' => false],
            'attributes.cron'  => ['required' => false],
        ]
    ];


    /**
     * Retrieve a flow by ID.
     *
     * @param string $workspaceid An Id of the Workspace.
     * @param array  $page        Pagination setting.
     * @param array  $filter      Filter setting.
     * @param string $sort        Flow list sorting.
     * @param string $search      Search string.
     *
     * @return Flow
     * @see    https://api.elastic.io/docs/v2/#retrieve-all-flows
     */
    public function retrieveAll(
        string $workspaceid,
        array $page=[],
        array $filter=[],
        string $sort=null,
        string $search=null
    ) {
        $queryData = ['workspace_id' => $workspaceid];
        foreach ($page as $key => $val) {
            $queryData['page['.$key.']'] = $val;
        }

        foreach ($filter as $key => $val) {
            $queryData['filter['.$key.']'] = $val;
        }

        if ($sort !== null) {
            $queryData['sort'] = $sort;
        }

        if ($search !== null) {
            $queryData['search'] = $search;
        }

        $this->response = $this->request
            ->setMethod('get')
            ->setPath($this->basePath.'?'.http_build_query($queryData))
            ->execute();
        return $this;

    }//end retrieveAll()


    /**
     * Retrieve a flow by ID.
     *
     * @param string $flowid Flow ID.
     *
     * @return Flow
     * @see    https://api.elastic.io/docs/v2/#retrieve-a-flow-by-id
     */
    public function retrieveByID(string $flowid)
    {
        $this->response = $this->request
            ->setMethod('get')
            ->setPath($this->basePath.'/'.$flowid)
            ->execute();
        return $this;

    }//end retrieveByID()


    /**
     * Retrieve a flow by ID.
     *
     * @param array $data     Parameters to create.
     * @param array $baseData Optional base data to use. Any missing parameter
     *                        in $data can be looked up from here.
     *
     * @return Flow
     * @see    https://api.elastic.io/docs/v2/#create-a-flow
     */
    public function create(array $data, array $baseData=null)
    {
        if ($baseData !== null) {
            $data = $this->mapData($this->parameters['create'], $data, $baseData);
        }

        $this->validateData($this->parameters['create'], $data);
        $this->response = $this->request
            ->setMethod('post')
            ->setPath($this->basePath)
            ->setData(['data' => $data])
            ->execute();
        return $this;

    }//end create()


}//end class