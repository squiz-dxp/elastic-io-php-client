<?php
/**
 * Credential Class.
 *
 * PHP Version 7.1+
 *
 * @package Squiz\ElasticIO
 * @author  Scott Kim <skim@squiz.net>
 */
namespace Squiz\ElasticIO\Model;

use Squiz\ElasticIO\Request;
use \Squiz\ElasticIO\Model\Base;

/**
 * Credential
 */
class Credential extends Base
{

    /**
     * Base API path for Credentials end-point.
     *
     * @var string
     */
    public $basePath = '/credentials';

    /**
     * List of API parameters.
     *
     * @var array
     */
    public $parameters = [
        'create' => [
            'type'                              => ['required' => true, 'type' => 'string', 'valid' => 'credential'],
            'attributes.name'                   => ['required' => false],
            'relationships.component.data.id'   => ['required' => true],
            'relationships.component.data.type' => ['required' => true, 'type' => 'string', 'valid' => 'component'],
            'relationships.workspace.data.id'   => ['required' => true],
            'relationships.workspace.data.type' => ['required' => true, 'type' => 'string', 'valid' => 'workspace'],
            'relationships.agent.data.id'       => ['required' => false],
            'relationships.agent.data.type'     => ['required' => false, 'type' => 'string', 'valid' => 'workspace'],
            'attributes.keys'                   => ['required' => false],
        ]
    ];


    /**
     * Retrieve a credential by ID.
     *
     * @param string $credentialid Credential ID.
     *
     * @return Credential
     * @see    https://api.elastic.io/docs/v2/#retrieve-a-credential-by-id
     */
    public function retrieveByID(string $credentialid)
    {
        $this->response = $this->request
            ->setMethod('get')
            ->setPath($this->basePath.'/'.$credentialid)
            ->execute();
        return $this;

    }//end retrieveByID()


    /**
     * Retrieve a credential by ID.
     *
     * @param array $data     Parameters to create.
     * @param array $baseData Optional base data to use. Any missing parameter
     *                        in $data can be looked up from here.
     *
     * @return Credential
     * @see    https://api.elastic.io/docs/v2/#create-a-credential
     */
    public function create(array $data, array $baseData=null)
    {
        if ($baseData !== null) {
            $data = $this->mapData($this->parameters['create'], $data, $baseData);
        }

        $this->validateData($this->parameters['create'], $data);
        $this->response = $this->request
            ->setMethod('post')
            ->setPath($this->basePath)
            ->setData(['data' => $data])
            ->execute();
        return $this;

    }//end create()


}//end class