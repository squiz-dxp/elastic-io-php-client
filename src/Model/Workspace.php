<?php
/**
 * Workspace Class.
 *
 * PHP Version 7.1+
 *
 * @package Squiz\ElasticIO
 * @author  Scott Kim <skim@squiz.net>
 */
namespace Squiz\ElasticIO\Model;

use Squiz\ElasticIO\Request;
use \Squiz\ElasticIO\Model\Base;

/**
 * Workspace
 */
class Workspace extends Base
{
    /**
     * Base API path for Workspaces end-point.
     *
     * @var string
     */
    public $basePath = '/workspaces';

    /**
     * List of API parameters.
     *
     * @var array
     */
    public $parameters = [
        'create' => [
            'type'                             => ['required' => true, 'type' => 'string', 'valid' => 'workspace'],
            'attributes.name'                  => ['required' => true],
            'relationships.contract.data.id'   => ['required' => true],
            'relationships.contract.data.type' => ['required' => true, 'type' => 'string', 'valid' => 'contract'],
        ],
        'addMember' => [
            'id'               => ['required' => true],
            'type'             => ['required' => true, 'type' => 'string', 'valid' => 'member'],
            'attributes.roles' => ['required' => true, 'type' => 'array'],
        ]
    ];


    /**
     * Creates a workspace.
     *
     * @param array $data     Parameters to create.
     * @param array $baseData Optional base data to use. Any missing parameter
     *                        in $data can be looked up from here.
     *
     * @return Workspace
     * @see    https://api.elastic.io/docs/v2/#create-a-workspace
     */
    public function create(array $data, array $baseData=null)
    {
        if ($baseData !== null) {
            $data = $this->mapData($this->parameters['create'], $data, $baseData);
        }

        $this->validateData($this->parameters['create'], $data);
        $this->response = $this->request
            ->setMethod('post')
            ->setPath($this->basePath)
            ->setData(['data' => $data])
            ->execute();
        return $this;

    }//end create()


    /**
     * Adds a new member to Workspace
     *
     * @param array   $data        Parameters to create.
     * @param array   $baseData    Optional base data to use. Any missing parameter
     *                             in $data can be looked up from here.
     * @param ?string $workspaceid Workspace ID.
     *
     * @return Workspace
     * @see    https://api.elastic.io/docs/v2/#add-a-new-member-to-workspace
     */
    public function addMember(array $data, array $baseData=null, ?string $workspaceid=null)
    {
        $workspaceid = $workspaceid ?? $this->response['data']['id'];
        if ($baseData !== null) {
            $data = $this->mapData($this->parameters['addMember'], $data, $baseData);
        }

        $this->validateData($this->parameters['addMember'], $data);
        $this->response = $this->request
            ->setMethod('post')
            ->setPath($this->basePath.'/'.$workspaceid.'/members')
            ->setData(['data' => $data])
            ->execute();
        return $this;

    }//end addMember()


    /**
     * Retrieve a workspace by ID.
     *
     * @param string $workspaceid Workspace ID.
     *
     * @return Workspace
     * @see    https://api.elastic.io/docs/v2/#get-workspace-by-id
     */
    public function getByID(string $workspaceid)
    {
        $this->response = $this->request
            ->setMethod('get')
            ->setPath($this->basePath.'/'.$workspaceid)
            ->execute();
        return $this;

    }//end getByID()


    /**
     * Delete a workspace by ID.
     *
     * @param string $workspaceid Workspace ID.
     *
     * @return Workspace
     * @see    https://api.elastic.io/docs/v2/#delete-workspace
     */
    public function deleteWorkspace(string $workspaceid)
    {
        $this->response = $this->request
            ->setMethod('delete')
            ->setPath($this->basePath.'/'.$workspaceid)
            ->execute();
        return $this;

    }//end deleteWorkspace()


}//end class