<?php
/**
 * Base Class.
 *
 * PHP Version 7.1+
 *
 * @package Squiz\ElasticIO
 * @author  Scott Kim <skim@squiz.net>
 */

namespace Squiz\ElasticIO\Model;

use Squiz\ElasticIO\Request;

/**
 * Base
 */
class Base
{

    /**
     * Request class object.
     *
     * @var object
     */
    protected $request = null;

    /**
     * Response data from ElasticIO API.
     *
     * @var array
     */
    protected $response = null;


    /**
     * Contructor.
     *
     * @param Request $request  Request object to talk to ElasticIO API.
     * @param array   $response Optional response value to set on initiation.
     *
     * @return void
     */
    public function __construct(Request $request, array $response=null)
    {
        $this->request  = $request;
        $this->response = $response;

    }//end __construct


    /**
     * Gets response data.
     *
     * @return array
     */
    public function getResponse()
    {
        return $this->response;

    }//end getResponse()


    /**
     * Gets JSON encoded string of the reponse.
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            $this->response,
            JSON_FORCE_OBJECT|JSON_PRETTY_PRINT
        );

    }//end __toString()


    /**
     * Returns true if the nested key exists in the array.
     *
     * @param string $key Flattened multi-demensional array key to check.
     * @param array  $arr Array to check.
     *
     * @return boolean
     */
    public function arrayMultiKeyExists(string $key, array $arr)
    {
        $parts    = explode('.', $key);
        $levelKey = array_shift($parts);
        if (count($parts) === 0) {
            return array_key_exists($levelKey, $arr);
        }

        if (array_key_exists($levelKey, $arr) === true) {
            return self::arrayMultiKeyExists(
                implode('.', $parts), $arr[$levelKey]
            );
        }

        return false;

    }//end arrayMultiKeyExists()


    /**
     * Gets the value of the nested key in the array.
     *
     * @param string $key Flattened multi-demensional array key to get value.
     * @param array  $arr Array to get value.
     *
     * @return mixed
     */
    public function arrayMultiKeyValue(string $key, array $arr)
    {
        eval('$value = $arr["'.str_replace('.', '"]["', $key).'"];');
        return $value;

    }//end arrayMultiKeyValue()


    /**
     * Sets the value of the nested key in the array.
     *
     * @param string $key Flattened multi-demensional array key to set value.
     * @param array  $arr Array to set value on.
     *
     * @return mixed
     */
    public function arrayMultiKeySet(string $key, $value, array $arr)
    {
        eval('$arr["'.str_replace('.', '"]["', $key).'"] = $value;');
        return $arr;

    }//end arrayMultiKeyValue()


    /**
     * Validate data array against parameters list.
     *
     * @param array $parameters List of flattened multi-demensional array key
     *                          and its value datails to validate.
     * @param array $data       Array to validate.
     *
     * @return void
     * @throws \Exception Thrown when validation fails.
     */
    public function validateData(array $parameters, array $data)
    {
        foreach ($parameters as $paramKey => $paramInfo) {
            if ($this->arrayMultiKeyExists($paramKey, $data) === true) {
                if (isset($paramInfo['type']) === false) {
                    continue;
                }

                $value     = $this->arrayMultiKeyValue($paramKey, $data);
                $valueType = gettype($value);
                if ($paramInfo['type'] !== $valueType) {
                    throw new \Exception(sprintf(
                        'Invalid value type for "%s". Expected "%s" but found "%s"',
                        $paramKey,
                        $paramInfo['type'],
                        $valueType
                    ));
                }

                if (isset($paramInfo['valid']) === false) {
                    continue;
                }

                if (is_array($paramInfo['valid']) === true) {
                    if (in_array($value, $paramInfo['valid']) === false) {
                        throw new \Exception(sprintf(
                            'Invalid value for "%s". Expected one of [%s] but found "%s"',
                            $paramKey,
                            implode(', ', $paramInfo['valid']),
                            $value
                        ));
                    }
                } else if ($value !== $paramInfo['valid']) {
                    throw new \Exception(sprintf(
                        'Invalid value for "%s". Expected "%s" but found "%s"',
                        $paramKey,
                        $paramInfo['valid'],
                        $value
                    ));
                }//end if
            } else if ($paramInfo['required'] === true) {
                throw new \Exception(
                    sprintf('Failed to validate: "%s" not found', $paramKey)
                );
            }//end if
        }//end foreach

    }//end validateData()


    /**
     * Helper function to create multi-dimensional array from given parameters list.
     *
     * @param array $parameters List of flattened multi-demensional array key
     *                          and its value datails to validate.
     * @param array $data       The array to return as a result. It should be
     *                          validated against parameters list.
     * @param array $baseData
     *
     * @return array
     */
    public function mapData(array $parameters, array $data, array $baseData=null)
    {
        $newData = [];
        foreach ($parameters as $paramKey => $paramInfo) {
            if ($this->arrayMultiKeyExists($paramKey, $data) === true) {
                $newData = $this->arrayMultiKeySet(
                    $paramKey,
                    $this->arrayMultiKeyValue($paramKey, $data),
                    $newData
                );
            } else if ($this->arrayMultiKeyExists($paramKey, $baseData) === true) {
                $newData = $this->arrayMultiKeySet(
                    $paramKey,
                    $this->arrayMultiKeyValue($paramKey, $baseData),
                    $newData
                );
            } else if ($paramInfo['required'] === true) {
                throw new \Exception(
                    sprintf('Failed to map data: "%s" not found', $paramKey)
                );
            }
        }//end foreach

        return $newData;

    }//end mapData()


}//end class