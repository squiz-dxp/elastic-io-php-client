<?php
/**
 * Factory Class.
 *
 * PHP Version 7.1+
 *
 * @package Squiz\ElasticIO
 * @author  Scott Kim <skim@squiz.net>
 */
namespace Squiz\ElasticIO;

use Squiz\ElasticIO\Request;
use Squiz\ElasticIO\Model\Workspace;
use Squiz\ElasticIO\Model\Component;
use Squiz\ElasticIO\Model\Credential;
use Squiz\ElasticIO\Model\Flow;
use Squiz\ElasticIO\Model\DataSample;

/**
 * Factory
 */
class Factory
{


    /**
     * Returns a requested type of object.
     *
     * @param string $type     Type of object to get.
     * @param string $username Username for ElasticIO admin API. If not given
     *                         it uses a defined constant.
     * @param string $apikey   API Key for ElasticIO admin API. If not given
     *                         it uses a defined constant.
     *
     * @return object
     */
    public static function get(string $type, string $username=null, string $apikey=null)
    {
        $request = new Request(($username ?? USERNAME), ($apikey ?? APIKEY));
        switch ($type) {
            case 'workspace':
                return new Workspace($request);

            case 'component':
                return new Component($request);

            case 'credential':
                return new Credential($request);

            case 'flow':
                return new Flow($request);

            case 'dataSample':
                return new DataSample($request);

            default:
                throw new \Exception('Unknown type');
            break;
        }//end switch

    }//end get()


}//end class