<?php
/**
 * Request Class.
 *
 * PHP Version 7.1+
 *
 * @package Squiz\ElasticIO
 * @author  Scott Kim <skim@squiz.net>
 */
namespace Squiz\ElasticIO;

/**
 * Request
 */
class Request
{

    /**
     * Constructor.
     *
     * @param string $username Username for ElasticIO admin API.
     * @param string $apikey   API Key for ElasticIO admin API.
     */
    public function __construct(string $username, string $apikey)
    {
        $this->apiurl   = 'https://api.elastic.io/v2';
        $this->path     = '';
        $this->username = $username;
        $this->apikey   = $apikey;

    }//end __construct()


    /**
     * Sets request path.
     *
     * @param string $path URL path to append.
     *
     * @return Request
     */
    public function setPath(string $path)
    {
        $this->path = $path;
        return $this;

    }//end setPath()


    /**
     * Sets request method.
     *
     * @param string $path Request method to use.
     *
     * @return Request
     */
    public function setMethod(string $method)
    {
        $this->method = $method;
        return $this;

    }//end setMethod()


    /**
     * Sets post fields data.
     *
     * @param array $data Request data.
     *
     * @return Request
     */
    public function setData(array $data)
    {
        $this->data = $data;
        return $this;

    }//end setData()


    /**
     * Sends cUrl request.
     *
     * @return array
     * @throws \Exception Thrown when error occurs.
     */
    public function execute()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->apiurl.$this->path);
        curl_setopt($curl, CURLOPT_USERPWD, $this->username . ':' . $this->apikey);

        $headers = [
            'Cache-Control: no-cache',
            'Connection: keep-alive',
            'accept-encoding: gzip, deflate',
            'cache-control: no-cache',
        ];

        switch ($this->method) {
            case 'get':
                $headers[] = 'Accept: */*';
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
            break;

            case 'post':
            case 'patch':
                $headers[] = 'Accept: application/json';
                $headers[] = 'Content-Type: application/json';
                curl_setopt($curl, CURLOPT_POST, 1);
                if (empty($this->data) === false) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($this->data, true));
                }
            break;

            case 'put':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
                if (empty($this->data) === false) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $this->data);
                }
            break;

            case 'delete':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
            break;
        }//end switch

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_ENCODING, '');
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $errMsg   = null;
        $response = curl_exec($curl);
        if ($response === false) {
            $errMsg = sprintf("Error: %s\ncURL Info: %s\n", curl_error($curl), var_export(curl_getinfo($curl), 1));
        }

        if ($errMsg !== null) {
            curl_close($curl);
            throw new \Exception($errMsg);
        }

        $result = json_decode($response, true);

        if (DEBUG) {
            $header = sprintf('[%s] %s', strtoupper($this->method), $this->apiurl.$this->path);
            echo str_repeat('-', strlen($header))."\n".$header."\n".str_repeat('-', strlen($header))."\n";
            if (empty($this->data) === false) {
                echo var_export($this->data, 1)."\n";
            }
            echo var_export($result, 1)."\n";
        }

        $this->data = null;
        curl_close($curl);
        return $result;

    }//end execute()

}//end class