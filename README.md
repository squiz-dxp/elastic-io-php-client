PSR-4 compliant PHP client for ElasticIO's admin API Version 2

How to install
--------------

* Requires minimum PHP version > 7.1
* `composer install`


Example Usage
-------------

```php
// Username and API Key needs to be defined.
define('USERNAME', '');
define('APIKEY', '');

// Retrieving an exsting flow.
$flowid = 'FLOWID_TO_RETRIEVE';
$json   = Factory::get('flow')->retrieveByID($flowid)->getResponse();

// Creating a new workspace.
$json = Factory::get('workspace')->create(
    [
        'type'          => 'workspace',
        'attributes'    => ['name' => 'New Workspace Name'],
        'relationships' => [
            'contract' => [
                'data' => [
                    'id'   => $contractid,
                    'type' => 'contract',
                ]
            ]
        ]
    ]
)->getResponse();
```
